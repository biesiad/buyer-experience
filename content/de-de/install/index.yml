---
  title: Laden Sie GitLab herunter und installieren Sie es
  description: Laden Sie Ihre eigene GitLab-Instanz herunter, installieren und warten Sie sie mit verschiedenen Installationspaketen und Downloads für Linux, Kubernetes, Docker, Google Cloud und mehr.
  side_menu:
    anchors:
      text: "Auf dieser Seite"
      data:
      - text: Offizielles Linux-Paket
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Kubernetes-Bereitstellungen
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: Unterstützte Cloud
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: Andere offizielle Methoden
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: Community-Beitrag
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: Bereits installiert?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "Mehr zu diesem Thema"
      data:
        - text: "Holen Sie sich die kostenlose ultimative Testversion"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "Selbstverwaltet installieren"
          href: "/de-de/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "Beginnen Sie mit SaaS"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "Kauf auf Marktplätzen"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: Installieren Sie selbstverwaltetes GitLab
    subtitle: Probieren Sie GitLab noch heute aus. Laden Sie Ihre eigene GitLab-Instanz herunter, installieren und warten Sie sie.
    text: |
      [Testversion](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} – starten Sie noch heute Ihre kostenlose ultimative Testversion

      [Selbstverwaltet](/de-de/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} – Installation auf Ihrer eigenen Infrastruktur

      [SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"}  – beginnen Sie mit unserem SaaS-Angebot

      [Marketplace](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"}  – kaufen Sie nahtlos über den Cloud-Marktplatz Ihrer Wahl ein
  linux:
    title: Offizielles Linux-Paket
    subtitle: Empfohlene Installationsmethode
    text: |
      Dies ist die empfohlene Methode für den Einstieg. Die Linux-Pakete sind ausgereift, skalierbar und werden heute auf GitLab.com verwendet. Wenn Sie zusätzliche Flexibilität und Widerstandsfähigkeit benötigen, empfehlen wir die Bereitstellung von GitLab wie in der [Referenzarchitekturdokumentation.](https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"}

      Die Linux-Installation ist schneller zu installieren, einfacher zu aktualisieren und enthält Funktionen zur Verbesserung der Zuverlässigkeit, die bei anderen Methoden nicht zu finden sind. Installieren Sie über ein einziges Paket (auch bekannt als Omnibus), das alle verschiedenen Dienste und Tools bündelt, die zum Ausführen von GitLab erforderlich sind. Mindestens 4\_GB RAM werden empfohlen ([Mindestanforderungen](https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).
    cards:
      - title: Ubuntu
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu
          variant: marketing
          alt: Ubuntu Icon
          hex_color: '#E95420'
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#ubuntu
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: Debian
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#debian
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: AlmaLinux
        id: almalinux
        subtext: Versionen 8, 9 für RHEL-kompatible Distributionen
        icon:
          name: almalinux
          variant: marketing
          alt: Almalinux Icon
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#almalinux
        data_ga_name: almalinux installation documentation
        data_ga_location: linux installation
      - title: CentOS 7
        id: centos-7
        subtext: and RHEL, Oracle, Scientific
        icon:
          name: centos
          variant: marketing
          alt: CentOs Icon
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#centos-7
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4 and SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          variant: marketing
          alt: OpenSuse Icon
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#opensuse-leap
        data_ga_name: opensuse leap installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#amazonlinux-2
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2022
        id: amazonlinux-2022
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#amazonlinux-2022
        data_ga_name: amazonlinux 2022 installation documentation
        data_ga_location: linux installation

      - title: Raspberry Pi-Betriebssystem
        id: raspberry-pi-os
        subtext: Bullseye und Buster (32 Bit)
        icon:
          name: raspberry-pi
          variant: marketing
          alt: Raspberry Pi Icon
          hex_color: '#000'
        link_text: Installationsanweisungen anzeigen +
        link_url: /de-de/install/#raspberry-pi-os
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          Für Ubuntu 20.04 und 22.04 sind auch „arm64“-Pakete verfügbar und werden automatisch auf dieser Plattform verwendet, wenn das GitLab-Repository für die Installation verwendet wird.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          Für Debian 10 sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn das GitLab-Repository für die Installation verwendet wird.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux
        tip: |
          Für AlmaLinux und RedHat Versionen 8 und 9 sind auch „arm64“-Pakete verfügbar und werden automatisch auf dieser Plattform verwendet, wenn das GitLab-Repository zur Installation verwendet wird.
        dependency_text: Auf AlmaLinux (und RedHat) Versionen 8 und 9 öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugriff in der System-Firewall. Dies ist ein optionaler Schritt, den Sie überspringen können, wenn Sie nur über Ihr lokales Netzwerk auf GitLab zugreifen möchten.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # OpenSSH-Server-Daemon aktivieren, falls nicht aktiviert: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Prüfen, ob das Öffnen der Firewall erforderlich ist mit: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: almalinux
      - id: centos-7
        dependency_text: Unter CentOS 7 (und RedHat/Oracle/Scientific Linux 7) öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugriff in der Systemfirewall. Dies ist ein optionaler Schritt, den Sie überspringen können, wenn Sie nur über Ihr lokales Netzwerk auf GitLab zugreifen möchten.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
          # OpenSSH-Server-Daemon aktivieren, falls nicht aktiviert: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Prüfen, ob das Öffnen der Firewall erforderlich ist mit: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: centos-7
      - id: opensuse-leap
        tip: |
          Für OpenSuse sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn das GitLab-Repository für die Installation verwendet wird.
        dependency_text: Unter OpenSUSE öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugriff in der System-Firewall. Dies ist ein optionaler Schritt, den Sie überspringen können, wenn Sie nur über Ihr lokales Netzwerk auf GitLab zugreifen möchten
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # OpenSSH-Server-Daemon aktivieren, falls nicht aktiviert: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Prüfen Sie, ob das Öffnen der Firewall erforderlich ist mit: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2022
        tip: |
          Für Amazon Linux 2022 sind „arm64“-Pakete ebenfalls verfügbar und werden auf dieser Plattform automatisch verwendet, wenn das GitLab-Repository für die Installation verwendet wird.
        dependency_text: Unter Amazon Linux 2022 öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugriff in der System-Firewall. Dies ist ein optionaler Schritt, den Sie überspringen können, wenn Sie nur über Ihr lokales Netzwerk auf GitLab zugreifen möchten.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # OpenSSH-Server-Daemon aktivieren, falls nicht aktiviert: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Prüfen, ob das Öffnen der Firewall erforderlich ist mit: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: amazonlinux-2022

      - id: amazonlinux-2
        tip: |
          Für Amazon Linux 2 sind auch „arm64“-Pakete verfügbar und werden automatisch auf dieser Plattform verwendet, wenn das GitLab-Repository für die Installation verwendet wird.
        dependency_text: Unter Amazon Linux 2 öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugriff in der System-Firewall. Dies ist ein optionaler Schritt, den Sie überspringen können, wenn Sie nur über Ihr lokales Netzwerk auf GitLab zugreifen möchten.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # OpenSSH-Server-Daemon aktivieren, falls nicht aktiviert: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Überprüfen Sie, ob das Öffnen der Firewall wird benötigt mit: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: amazonlinux-2
      - id: raspberry-pi-os
        tip: |
          Raspberry Pi 4 mit mindestens 4GB wird empfohlen. Derzeit wird nur 32bit (armhf) unterstützt. 64 Bit (`arm64`) ist auf dem Weg.
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Kubernetes-Bereitstellungen
    text: |
      Bei der Installation von GitLab auf Kubernetes müssen Sie einige Kompromisse beachten:

        - Verwaltung und Fehlerbehebung erfordern Kubernetes-Kenntnisse
        - Bei kleineren Installationen kann es teurer werden. Die Standardinstallation erfordert mehr Ressourcen als die Bereitstellung eines Linux-Pakets mit einem einzelnen Knoten, da die meisten Dienste redundant bereitgestellt werden.
        - Es gibt einige Funktionen [Einschränkungen, die Sie beachten sollten.](https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"}

      Verwenden Sie diese Methode, wenn Ihre Infrastruktur auf Kubernetes basiert und Sie mit der Funktionsweise vertraut sind. Die Methoden für Verwaltung, Beobachtbarkeit und einige Konzepte unterscheiden sich von herkömmlichen Bereitstellungen. Die Helm-Chart-Methode ist für Vanilla Kubernetes-Bereitstellungen und der GitLab-Operator kann verwendet werden, um GitLab auf einem OpenShift-Cluster bereitzustellen. Der GitLab-Operator kann verwendet werden, um Day-2-Vorgänge in OpenShift- und Vanilla-Kubernetes-Bereitstellungen zu automatisieren.
    cards:
      - title: Helm-Diagramm
        subtext: Installieren Sie GitLab mithilfe von Helm-Diagrammen
        icon:
          name: kubernetes
          variant: marketing
          alt: Kubernetes Icon
          hex_color: '#336CE4'
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: GitLab-Operator
        new_flag: true
        subtext: Installieren Sie GitLab mit dem Operator
        icon:
          name: gitlab-operator
          variant: marketing
          alt: GitLab Operator Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: Unterstützte Cloud
    text: |
      Verwenden Sie das offizielle Linux-Paket, um GitLab bei verschiedenen Cloud-Anbietern zu installieren
    cards:
      - title: Amazon Web Services (AWS)
        subtext: Installieren Sie GitLab auf AWS
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud-Plattform (GCP)
        subtext: Installieren Sie GitLab auf der GCP
        icon:
          name: gcp
          variant: marketing
          alt: GCP Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: Microsoft Azure
        subtext: Installieren Sie GitLab auf Azure
        icon:
          name: azure
          variant: marketing
          alt: Azure Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: Andere offizielle, unterstützte Installationsmethoden
    cards:
      - title: Docker
        subtext: Offizielle GitLab Docker-Images
        icon:
          name: docker
          variant: marketing
          alt: Docker Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: Referenzarchitekturen
        subtext: Empfohlene GitLab-Bereitstellungstopologien
        icon:
          name: gitlab
          variant: marketing
          alt: GitLab Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: Installation von der Quelle
        subtext: Installieren Sie GitLab mithilfe der Quelldateien auf einem Debian/Ubuntu-System
        icon:
          name: source
          variant: marketing
          alt: Source Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: GitLab Environment Toolkit (GET)
        subtext: Automatisierung für die Bereitstellung von GitLab-Referenzarchitekturen mit Terraform und Ansible
        icon:
          name: gitlab-environment-toolkit
          variant: marketing
          alt: GitLab Environment Toolkit Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: Inoffizielle, nicht unterstützte Installationsmethoden
    cards:
      - title: Debian-natives Paket
        subtext: von Pirat Praveen
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: Installationsanweisungen anzeigen
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: FreeBSD package
        subtext: von Torsten Zühlsdorff
        icon:
          name: freebsd
          variant: marketing
          alt: FreeBSD Icon
        link_text: Installationsanweisungen anzeigen
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd installation
        data_ga_location: unofficial installation
      - title: Arch-Linux-Paket
        subtext: von der Arch Linux-Community
        icon:
          name: arch-linux
          variant: marketing
          alt: Arch Linux Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://www.archlinux.org/packages/community/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: Puppet-Modul
        subtext: von Vox Pupuli
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          variant: marketing
          alt: Puppet Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: Ansible-Playbook
        subtext: von Jeff Geerling
        icon:
          name: ansible
          variant: marketing
          alt: Ansible Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: Virtuelle GitLab-Appliance (KVM)
        subtext: von OpenNebula
        icon:
          name: open-nebula
          variant: marketing
          alt: Open Nebula Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab auf Cloudron
        subtext: über die Cloudron-App-Bibliothek
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          variant: marketing
          alt: Cloudron Icon
        link_text: Installationsanweisungen anzeigen
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: GitLab bereits installiert?
    cards:
      - title: Update von einer alten Version von GitLab
        text: Aktualisieren Sie Ihre GitLab-Installation, um die neuesten Funktionen zu nutzen. Versionen von GitLab, die neue Funktionen enthalten, werden jeden Monat am 22. veröffentlicht
        link_url: https://about.gitlab.com/update/
        link_text: Aktualisieren Sie auf die neueste Version von GitLab
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: Update von GitLab Community Edition
        text: Die GitLab Enterprise Edition enthält erweiterte Features und Funktionen, die in der Community Edition nicht verfügbar sind.
        link_url: https://about.gitlab.com/upgrade/
        link_text: Aktualisieren Sie auf die Enterprise-Edition
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: Upgrade vom manuell installierten Omnibus-Paket
        text: Mit GitLab 7.10 haben wir Paket-Repositorys für GitLab eingeführt, die es Ihnen ermöglichen, GitLab mit einem einfachen Befehl zu installieren.
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: Aktualisieren Sie auf das Omnibus-Paket-Repository
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: Anwendungen von Drittanbietern, die GitLab unterstützenAnwendungen von Drittanbietern, die GitLab unterstützen
        text: GitLab ist offen für Zusammenarbeit und engagiert sich für den Aufbau von Technologiepartnerschaften im DevOps-Ökosystem. Erfahren Sie mehr über die Vorteile und Anforderungen, ein GitLab-Technologiepartner zu werden.
        link_url: https://about.gitlab.com/partners/
        link_text: Zeigen Sie Anwendungen von Drittanbietern an
        data_ga_name: View third-party applications
        data_ga_location: update
