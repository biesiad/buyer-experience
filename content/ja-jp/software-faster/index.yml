---
  title:  ソフトウェア。もっと速く
  description: GitLabはDevSecOpsを簡素化し、重要なことに集中できるようにします。詳細はこちら!
  image_title: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      アイデアからソフトウェアへのより速いパス
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: software-faster hero image
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: GitLabとは?
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: T-Mobile と GitLab の Web キャストのランディング ページへのリンク
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Goldman Sachsお客様事例へのリンク
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Cloud Native Computing Foundation のお客様のケース スタディへのリンク
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Siemensお客様事例へのリンク
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Nvidiaお客様事例へのリンク
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: UBSお客様事例へのリンク
  featured_content:
    col_size: 4
    header: "連携でさらに向上: GitLabでお客様はソフトウェアをより速く提供"
    case_studies:
      - header: ナスダックのクラウドへの迅速かつシームレスな移行
        description: |
          ナスダックは、100 ％クラウドにするというビジョンを持っています。そこにたどり着くためにGitLabと提携しています。
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: 動画を視聴
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: デプロイ時間を5倍速く
        description: |
          HackeroneはGitLab Ultimateを使用してパイプラインの時間、デプロイ速度、開発者の効率を改善しました。
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Person working on a computer with code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: 詳細はこちら
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: 144倍速く機能をリリース
        description: |
          Airbus Intelligenceは、単一のアプリケーションCIでワークフローとコード品質を改善しました。
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Airplane wing on flight - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: ストーリーを読む
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: 1つの包括的なプラットフォーム内で重要なDevSecOpsツールをすべて提供
    link:
      text: 詳細はこちら
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: より優れたインサイト
        description: ソフトウェアデリバリーライフサイクル全体のエンドツーエンドの可視性。
        icon: case-study-alt
      - header: 効率アップ
        description: 自動化と第3のサービスとの統合のための組み込みサポート。
        icon: principles
      - header: 連携の改善
        description: 開発者、セキュリティ、および運用チームを統合する1つのワークフロー。
        icon: roles
      - header: 価値を生み出すまでの時間を短縮
        description: 加速されたフィードバックループによる継続的な改善。
        icon: verification
  by_industry_case_studies:
    title: DevSecOpsリソースについて
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2023年グローバルDevSecOpsレポートシリーズ
        subtitle: 5,000人以上のDevSecOpsプロフェッショナルから、ソフトウェア開発、セキュリティ、および運用の現状について学んだことをご覧ください。
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: レポートを読む
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: '歩調を合わせることがすべて: 10 人のエンジニアのために10個のエンジニアリング組織'
        subtitle: GitLabのCEOで共同創業者であるSid Sijbrandijが、エンジニアリング組織において、歩調を合わせることの重要性について語りました。
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: picture of people running marathon
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: ブログを読む
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon
