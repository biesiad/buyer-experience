---
  title: "DevSecOps for Small Business - コラボレーションを簡単に"
  description: "GitLabのDevSecOpsプラットフォームを使用してソフトウェアデリバリーを加速し、開発コストを削減して、チームコラボレーションを合理化"
  image_title: "/nuxt-images/open-graph/gitlab-smb-opengraph.png"
  canonical_url: "/small-business/"
  side_navigation_links:
    - title: 概要
      href: '#overview'
    - title: 機能
      href: '#capabilities'
    - title: メリット
      href: '#benefits'
    - title: ケーススタディ
      href: '#case-studies'
  solutions_hero:
    title: GitLab for Small Businesses
    subtitle: DevSecOpsプラットフォームは、チームをまとめ、必要なものがすべて組み込まれています。
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Ultimateを無料で試用
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: join gitlab
      data_ga_location: header
    secondary_btn:
      text: 料金について
      url: /pricing/
      data_ga_name: Learn about pricing
      data_ga_location: header
    image:
      image_url: /nuxt-images/resources/resources_19.jpg
      alt: "会議を上から見た図"
      rounded: true
  by_industry_intro:
    logos:
      - name: Hotjar
        image: /nuxt-images/logos/hotjar-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/hotjar/
        aria_label: Hotjarお客様事例へのリンク
      - name: Chorus
        image: /nuxt-images/home/logo_chorus_color.svg
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/chorus/
        aria_label: Chorusお客様事例へのリンク
      - name: Anchormen
        image: /nuxt-images/logos/anchormen-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/anchormen/
        aria_label: Anchormenお客様事例へのリンク
      - name: Remote
        image: /nuxt-images/logos/remote-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/remote/
        aria_label: Remoteお客様事例へのリンク
      - name: Glympse
        image: /nuxt-images/logos/glympse-logo-mono.svg
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/glympse/
        aria_label: G Olympseお客様事例へのリンク
      - name: FullSave
        image: /nuxt-images/case-study-logos/fullsave-logo.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/fullsave/
        aria_label: FullSaveお客様事例へのリンク
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: スモールビジネスではやらなければならないことがたくさんあります。
      description: DevSecOpsソリューションは、解決するよりも多くの問題を生み出すべきではありません。ポイントソリューション上に構築された脆弱なツールチェーンとは異なり、GitLabはチームがより速くイテレーションを行い、一緒に革新し、複雑さとリスクを排除して、より高品質でより安全なソフトウェアをより速く提供するために必要なものをすべて提供します。
  by_solution_benefits:
    title: 大規模なDevSecOps
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    video:
      video_url: "https://player.vimeo.com/video/703370435?h=fc07a70b24&color=7759C2&title=0&byline=0&portrait=0"
    items:
      - icon:
          name: continuous-integration
          alt: 継続的インテグレーション アイコン
          variant: marketing
          hex_color: "#171321"
        header: さっそく始めましょう
        text: すべてのDevSecOpsプロセスを一元管理するために必要な独自のすべてのもの、迅速に開始できるテンプレート、およびベストプラクティスが組み込まれています。
      - icon:
          name: continuous-integration
          alt: 継続的インテグレーション アイコン
          hex_color: "#171321"
          variant: marketing
        header: DevSecOpsを簡素化
        text: チームは、ツールチェーンの統合を維持するのではなく、価値を提供することに焦点をあてる必要があります。
      - icon:
          name: auto-scale
          alt: Auto Scale Icon
          hex_color: "#171321"
          variant: marketing
        header: Enterprise-ready
        text: ビジネスが拡張するにつれて、DevSecOpsプラットフォームも拡張されますが、複雑さが増すことはありません。
      - icon:
          name: devsecops
          alt: DevSecOps Icon
          hex_color: "#171321"
          variant: marketing
        header: リスクとコストの削減
        text: スピードや費用を犠牲にすることなく、セキュリティとコンプライアンスを自動化して実施します。
  by_industry_solutions_block:
    subtitle: 主要な機能
    sub_description: "DevSecOpsプラットフォームは、真のエンドツーエンドのサポートを提供し、最小限の摩擦で最大限の顧客価値を提供するのに役立ちます。主要な機能は以下になります。"
    white_bg: true
    markdown: true
    sub_image: /nuxt-images/small-business/no-image-alternative-export.svg
    alt: 複数のウィンドウの画像
    solutions:
      - title: GitLab Free
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、GitOpsが使いやすい１つのプラットフォームで提供


           **基本的な問題管理**

          イシュー(別名ストーリー)を作成し割り当てて、進捗状況を追跡する


          **基本的なセキュリティスキャン**

          静的アプリケーションセキュリティテスト(SAST)およびシークレット検出
        link_text: 詳細はこちら
        link_url: /pricing/
        data_ga_name: gitlab free
        data_ga_location: body
      - title: GitLab プレミアム
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、GitOpsが使いやすい１つのプラットフォームで提供


           **基本的な問題管理**

          イシュー(別名ストーリー)を作成し割り当てて、進捗状況を追跡する


          **基本的なセキュリティスキャン**

          静的アプリケーションセキュリティテスト(SAST)およびシークレット検出
        link_text: 詳細はこちら
        link_url: /pricing/premium/
        data_ga_name: gitlab premium
        data_ga_location: body
      - title: GitLab Ultimate
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、およびGitOpsを1つの使いやすいプラットフォームで提供し、包括的な管理機能で拡張を支援します。


          **アジャイル計画**

          イシュー、マルチレベルエピック、バーンダウンチャートなどを含むプロジェクトプランニング


          **包括的なセキュリティテスト**

          SAST、シークレット検出、DAST、コンテナ、依存関係、クラスタイメージ、API、ファズテスト、ライセンスコンプライアンスを含む包括的なアプリケーションセキュリティテスト。


          **脆弱性管理**

          脆弱性評価、トリアージ、および修復のための実用的なダッシュボードのセキュリティおよびコンプライアンスの欠陥を参照してください。


          **ガバナンス**

          コンプライアンスパイプラインでポリシーとセキュリティガードを自動化します。


          **価値管理**

          エンドツーエンドのメトリクスでソフトウェア開発のスピードと結果を向上します。
        link_text: 詳細はこちら
        link_url: /pricing/ultimate/
        data_ga_name: gitlab ultimate
        data_ga_location: body
  by_solution_value_prop:
    title: 開発、SEC、運用のための1つのプラットフォーム
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: SCM
        description: バージョン管理、連携、基本的なストーリープランニングのためのソースコード管理。
        href: /stages-devops-lifecycle/source-code-management/
        cta: 詳細
        icon:
          name: cog-code
          alt: 歯車コード アイコン
          variant: marketing
      - title: CI/CD
        description: Auto DevOpsとの継続的な統合とデリバリー。
        href: /features/continuous-integration/
        cta: 詳細
        icon:
          name: continuous-delivery
          alt: 継続的デリバリー アイコン
          variant: marketing
      - title: GitOps
        description: クラウドネイティブ環境の複雑さを抽象化するインフラストラクチャの自動化。
        href: /solutions/devops-platform/
        cta: 詳細
        icon:
          name: automated-code
          alt: 自動コード アイコン
          variant: marketing
      - title: セキュリティ
        description: 包括的なセキュリティスキャンと脆弱性管理はいつでも使用可能。
        href: /solutions/security-compliance/
        cta: 詳細
        icon:
          name: shield-check
          alt: シールド チェック アイコン
          variant: marketing
  by_industry_case_studies:
    title: お客様が気づいたメリット
    link:
      text: すべてのケーススタディ
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Anchormen
        subtitle: GitLab CI/CDがAnchormenのイノベーションをどのようにサポートし、加速するのか
        image:
          url: /nuxt-images/blogimages/anchormen.jpg
          alt: たくさんのライトがある建物の中
        button:
          href: /customers/anchormen/
          text: 詳細はこちら
          data_ga_name: anchormen learn more
          data_ga_location: body
      - title: Glympse
        subtitle: Glympseで地理的に離れている共有を簡単にする
        image:
          url: /nuxt-images/blogimages/glympse_case_study.jpg
          alt: 街並みの画像
        button:
          href: /customers/glympse/
          text: 詳細はこちら
          data_ga_name: glympse learn more
          data_ga_location: body
      - title: MGA
        subtitle: どのようにGitLabでMGAがプロジェクトを5倍速く構築したのか
        image:
          url: /nuxt-images/blogimages/covermga.jpg
          alt: night traffic
        button:
          href: /customers/mga/
          text: 詳細はこちら
          data_ga_name: mga learn more
          data_ga_location: body
      - title: Hotjar
        subtitle: Hotjar が GitLab でデプロイを 50% 高速化する方法
        image:
          url: /nuxt-images/blogimages/hotjar.jpg
          alt: トンネルの中
        button:
          href: /customers/hotjar/
          text: 詳細はこちら
          data_ga_name: hotjar learn more
          data_ga_location: body
      - title: Nebulaworks
        subtitle: そのようにNebulaworksが3つのツールをGitLabに置き換え、顧客のスピードと俊敏性を強化したのか
        image:
          url: /nuxt-images/blogimages/nebulaworks.jpg
          alt: ツール画像
        button:
          href: /customers/nebulaworks/
          text: 詳細はこちら
          data_ga_name: nabulaworks learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: リソース
    link:
      text: すべてのリソースを表示
    cards:
      - icon:
          name: ebook-alt
          alt: 電子ブック アイコン
          variant: marketing
        event_type: 電子書籍
        header: DevSecOpsをはじめるためのSMBガイド
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
        alt: ラップトップ イメージでの作業
        href: https://page.gitlab.com/resources-ebook-smb-beginners-guide-devops.html
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: blog-alt
          alt: ブログ アイコン
          variant: marketing
        event_type: ブログ投稿
        header: 中小企業がDevSecOpsプラットフォームのパワーを活用できる6つの方法
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
        alt: コード スニペット イメージ
        href: https://about.gitlab.com/blog/2022/04/12/6-ways-smbs-can-leverage-the-power-of-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: blog-alt
          alt: ブログ アイコン
          variant: marketing
        event_type: 学ぶ
        header: 1つのアプリケーションで安全なコード管理、構築、コードレビューをリード
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/zoopla_cover_image.jpg
        alt: 近所の画像
        href: https://learn.gitlab.com/smb-ci-1/leading-scm-ci-and-c
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: video
          alt: ビデオ アイコン
          variant: marketing
        event_type: ビデオ
        header: GitLab DevSecOpsプラットフォームのデモを見る
        link_text: デモを視聴
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: gitlab Devops イメージ
        href: https://learn.gitlab.com/smb-beginners-devops/oei67xcnxmk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: ケーススタディ アイコン
          variant: marketing
        event_type: ブログ
        header: DevOpsプラットフォームには、SMBやスタートアップが小さすぎることはありますか？
        link_text: 詳細はこちら
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: 上から見た木
        href: https://about.gitlab.com/blog/2022/04/06/can-an-smb-or-start-up-be-too-small-for-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 1200
