---
  title: Value Stream Mapping
  description: Value stream mapping is a great way to identify value within your process and help you minimize waste.
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  date_published: 2023-04-06
  date_modified: 2023-04-06
  topics_header:
      data:
        title:  Value Stream Mapping
        block:
          - metadata:
              id_tag: what-is-devops
            text: |
              Value stream mapping is a great way to identify value within your process and help you minimize waste.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevOps
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: Building a DevOps team
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is value stream mapping?
          href: "#what-is-value-stream-mapping"
          data_ga_name: What is value stream mapping?
          data_ga_location: side-navigation
        - text: The importance of value stream mapping
          href: "#the-importance-of-value-stream-mapping"
          data_ga_name: The importance of value stream mapping
          data_ga_location: side-navigation
        - text: How VSM applies to continuous delivery
          href: "#how-vsm-applies-to-continuous-delivery"
          data_ga_name: How VSM applies to continuous delivery
          data_ga_location: side-navigation
        - text: Why map your value stream?
          href: "#why-map-your-value-stream"
          data_ga_name: Why map your value stream?
          data_ga_location: side-navigation
        - text: Steps involved in value stream mapping
          href: "#steps-involved-in-value-stream-mapping"
          data_ga_name: Steps involved in value stream mapping
          data_ga_location: side-navigation
        - text: Driving DevOps with value stream mapping
          href: "#driving-dev-ops-with-value-stream-mapping"
          data_ga_name: Driving DevOps with value stream mapping
          data_ga_location: side-navigation
        - text: Tools and techniques used in value stream mapping
          href: "#tools-and-techniques-used-in-value-stream-mapping"
          data_ga_name: Tools and techniques used in value stream mapping
          data_ga_location: side-navigation
        - text: How can value stream mapping be used to improve process performance?
          href: "#how-can-value-stream-mapping-be-used-to-improve-process-performance"
          data_ga_name: How can value stream mapping be used to improve process performance?
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: What is value stream mapping?
          blocks:
            - column_size: 8
              text: |
                [Value stream mapping (VSM)](/solutions/value-stream-management/) is a management method that enables you to visualize and create detailed analyses of all the activities required to deliver a product to a customer. VSM is helpful in various industries, including software development, marketing, manufacturing, IT operations, and logistics.

                In software development, VSM tools focus on workflow management and provide easy access to products that improve data availability and flow across DevOps and CI/CD pipelines. These pipelines are transforming the IT industry by implementing value streams across development and operations.
      - name: topics-copy-block
        data:
          header: The importance of value stream mapping
          blocks:
            - column_size: 8
              text: |
                VSM uses flowcharts to represent activities step by step, giving you a clear picture of the flow of information. It allows you to visualize the current state and data and provide feedback on overall progress. In the case of unwanted activities, your team can strategize and create value where needed using the knowledge they obtained from VSM.

                Using VSM to map your workflow visually helps you identify any waste in the process that should either be reduced or completely removed. This lets you focus on value-adding activities and results in an efficient and productive workflow.
      - name: topics-copy-block
        data:
          header: How VSM applies to continuous delivery
          blocks:
            - column_size: 8
              text: |
                The [continuous delivery (CD)](/topics/ci-cd/) pipeline describes the automated activities and workflows needed to release new functionality accurately. CD channels benefit greatly from VSM implementation, as it improves the flow of value across the pipeline and provides excellent visibility into the problem or decision point areas.

                The first step to improving any value stream is mapping the current CD pipeline. VSM provides a better view of the crucial activities in the delivery process and the flow of value upstream. It enables you to better understand the pipeline’s current state and identify improvement opportunities.
      - name: topics-copy-block
        data:
          header: Why map your value stream?
          blocks:
            - column_size: 8
              text: |
                VSM provides value by:

                * Identifying key areas to improve to maximize value for the user

                * Providing a clear picture of the current and future state, helping you determine if any improvements are needed

                * Identifying wasteful activities, such as inappropriate processing, and their causes

                * Helping teams learn about the development process through visual aids and serving as an instrument for communication and collaboration
      - name: topics-copy-block
        data:
          header: Steps involved in value stream mapping
          blocks:
            - column_size: 8
              text: |
                Let’s take a look at the four steps involved in VSM.

                1.  **Identify the product you want to map and improve.** To start, identify the product that needs improvement and can profit from a more streamlined experience. Next, define the start and end of the key processes related to the product. Defining this early will draw your focus to the activities that are adding value to the process.

                2.  **Create the current state VSM (CSVSM).** This step involves collaborating with your entire team to build an understanding of your current state. Invite all parties directly involved with the product and gather as much information as possible. During this time, you can analyze your data closely, review what you have, diagnose the potential causes of waste, and identify opportunities for potential improvements.

                3.  **Create the future state VSM (FSVSM).** As you create the future state of your VSM, prioritize the processes that need to be improved in the future. Focus on critical functions that add value and keep an eye out for opportunities to reduce waste. If you need to add to or simplify a process, now’s the time.

                4.  **Move from current state to future state VSM.** As you start adapting to the new future state VSM, always maintain a visual connection between the current and the future state VSM. This will help you align your goals and practical progress whenever you return to the drawing board.
      - name: topics-copy-block
        data:
          header: Driving DevOps with value stream mapping
          blocks:
            - column_size: 8
              text: |
                DevOps is a competitive software development strategy that embraces lean manufacturing techniques. Driving DevOps with VSM makes the entire development process more productive and presents opportunities for improvements. Let’s take a look at the components of VSM and their corresponding impact on DevOps:

                * **Process map:** This visually describes the flow of work and a series of activities that produce an outcome. This is vital for the DevOps team as they plan their work and keep up with its progress.

                * **Information flow:** This shows how information is delivered along the workflow and can help the DevOps team identify areas of waste.

                * **Timeline:** This shows the cycle, wait, and down times. If managed well, it can help the DevOps team make better decisions for future improvements, speed up delivery times, and make the entire approach to development more productive.
      - name: topics-copy-block
        data:
          header: Tools and techniques used in value stream mapping
          blocks:
            - column_size: 8
              text: |
                Let’s take a look at some of the tools and techniques used in VSM.

                * **Process activity mapping:** This tool eliminates unnecessary activities, simplifies complex ones, and upgrades processes to reduce waste.

                * **Supply chain response matrix:** This tool focuses on time-based mapping to analyze activities and lead time within an organization. The lead time is the amount of time between the start of activities or a project and the delivery of its intended results.

                * **Quality filter mapping:** This tool identifies where quality problems exist in the workflow. The information is used to understand where there is waste and what improvements are needed.

                * **Decision point analysis:** The decision point is associated with learning about the entire workflow of the decision. This information drives the direction of decision-making rules. The decision point analysis is the extraction of decisions and detects the links between data related to the whole situation. This tool points to any variability in the workflow.
      - name: topics-copy-block
        data:
          header: How can value stream mapping be used to improve process performance?
          blocks:
            - column_size: 8
              text: |
                VSM helps you see the specific processes that add value to your product or service. It also aids in identifying communication disparities between teams and across tasks and other activities. VSM analysis verifies the relevance of the value added, and visualization enhances effective collaboration between team members.
      - name: topics-cta
        data:
          subtitle: GitLab Value Stream Management
          text: |
            Jumpstart VSM by utilizing a single end-to-end DevOps platform to get visibility throughout the entire lifecycle, without the “toolchain tax.” GitLab’s Value Stream Management helps businesses visualize their DevOps workstream, identify and target waste and inefficiencies, and take action to optimize those workstreams to deliver the highest possible velocity of value.
          column_size: 10
          cta_one:
            text: Learn more about GitLab Value Stream Management
            link: /solutions/value-stream-management/
            data_ga_name: Learn more about GitLab Value Stream Management
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: More on DevOps teams
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: blog Icon
            event_type: Blog
            header: Autoscale GitLab CI/CD runners and save 90% on EC2 costs
            image: /nuxt-images/blogimages/axway-case-study-image.png
            link_text: "Learn more"
            href: /blog/2017/11/23/autoscale-ci-runners/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Case Study
            header: "Auto DevOps 101: How we’re making CI/CD easier"
            image: /nuxt-images/blogimages/autodevops.jpg
            link_text: "Learn more"
            href: /blog/2019/10/07/auto-devops-explained/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: How a GitLab engineer changed the future of DevOps
            image: /nuxt-images/blogimages/scm-ci-cr.png
            link_text: "Learn more"
            href: /blog/2020/10/29/gitlab-hero-devops-platform/
            data_ga_name:
            data_ga_location: body
