---
  data:
    title: Axway
    description: To help customers better leverage their data, Axway stays ahead of the market by releasing new code faster, using GitLab for source code management
    og_image: /nuxt-images/blogimages/axway-case-study-image.png
    customer: Axway
    customer_logo: /nuxt-images/logos/axway-logo.svg
    customer_logo_full: true
    heading:  Axway realizes a 26x faster release cycle by switching from Subversion to GitLab
    key_benefits:
      - label: On-premises solution
        icon: user-laptop
      - label: World-class integrations
        icon: bulb
      - label: Simplified administration
        icon: cogs
    header_image: /nuxt-images/blogimages/axway-case-study-image.png
    customer_industry: Computer software
    customer_employee_count: 1,001 - 5,000
    customer_location: Paris, France and Phoenix, Arizona
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
    - stat: 600+
      label: developers onboarded
    - stat: "3,000"
      label: projects migrated
    - stat: 26x
      label: faster release cycles
    blurb: International information technology company Axway was looking for a way to improve collaboration among globally distributed software development teams.
    introduction: |
      With GitLab, Axway now has a single solution for source code management for the entire company, fostering collaboration between development and operations teams.
    quotes:
    - text: |
        GitLab offered the most advanced feature set on the market.
      author: Eric Labourdette
      author_role: Head of Global R&D Engineering Services
      author_company: Axway
    content:
      - title: Change agents tackling the toughest data challenges
        description: >-
            Inventors of the world’s first business rules engine, Règle du Jeu (RDJ),
            Axway has been helping businesses unlock the value of their data for
            nearly two decades. Their cloud-enabled data integration and engagement
            platform, Axway AMPLIFY, orchestrates cohesive customer experience
            networks, enabling organizations to generate the speed, power, and
            agility required to meet skyrocketing customer expectations.


            As change agents for data integration, keeping pace and adapting quickly
            to today’s fast and fluid digital customer is mission critical for
            Axway’s research and development (R&D) engineering services teams.
      - title: Legacy source code management and toolchain complexity limits worldwide collaboration
        description: >-
            Serving over 11,000 companies spanning 100 countries, Axway’s R&D
            engineering services teams are spread across continents and time zones.
            With the teams working from nine different sites and using Subversion
            (SVN) on local servers for source control management (SCM), collaboration
            between the globally distributed teams was suffering, preventing them
            from implementing DevOps.


            “Our legacy toolchain was complex, disconnected, and difficult to manage
            and maintain,” said Eric Labourdette, Head of Global R&D Engineering
            Services and responsible for worldwide R&D operations including DevOps.
            “SVN was difficult to administer and didn’t scale, causing extra
            administrative overhead. It became unmanageable for our team size.”


            As the organization looked to move to a microservices architecture and
            implement DevOps, they needed a solution for  SCM that could foster
            collaboration across locations and projects, and support DevOps
            practices like continuous integration and automated deploys.

      - title: GitLab for SCM enables DevOps
        description: >-
            A modern solution that can be hosted on-premises, GitLab provided a
            single instance of source code for the entire company. Leveraging a
            Git-based workflow, Axway’s worldwide R&D teams can now easily share and
            collaborate on code with nearly zero downtime and minimal administrative
            overhead. Leveraging the most robust Lightweight Directory Access
            Protocol (LDAP) support of any SCM provider, with GitLab, Axway is able to manage
            access and permissions for 600 enterprise developers securely and
            consistently.


            “GitLab met our requirements and gave us the best value for the price,”
            said Labourdette. “The feature set with GitLab on-premise was more
            advanced than GitHub and we saw the pace and development [of GitLab]
            moving faster with a community that was active in delivering and
            contributing.”


            Collaboration between development and operations teams improved, too.
            Using GitLab’s Jenkins integration, developers can trigger their own
            builds and see the output of the pipeline status in the same view as
            their code within GitLab.


            "All of the development we have done starts with central source code management
            in GitLab. This is followed with ephemeral Jenkins through GitLab CI to “eat our own dog
            food” in creating pipelines that build and deploy images then QA them and execute
            a security scan. Immutable infrastructure and common continuous deployment using
            Artifactory and Bintray repositories allows us to deploy Docker container images
            into AWS. Our new development is based on microservices, Docker on Kubernetes with
            GitLab being the source of our DevOps pipelines," Labourdette explains.


      - title: Faster release cycles and soaring adoption
        description: >-
            Now, with developers empowered to self-serve with minimal guidelines,
            the DevOps teams at Axway have improved their release cycle time from annual
            releases down to two weeks for some products.


            In using a bottom-up approach to choose their SCM solution, adoption of
            GitLab was unsurprisingly fast and natural. In just one year, 630 users
            were onboarded and 3,000 projects were migrated. “Everyone was asking
            for it,” said Labourdette. “Now, every single developer is running on
            GitLab without enforcement.”
