---
  title: ANWB
  description: ANWB has innovated travel for the past 135 years and is actively preparing for the next 135
  image_title: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
  data:
    customer: ANWB
    customer_logo: /nuxt-images/customers/anwb_logo.svg
    heading: From bicycles to connected driving
    key_benefits:
      - label: Improved collaboration
        icon: collaboration-alt-4
      - label: Improved CI/CD
        icon: accelerate
      - label: Increased team autonomy
        icon: bulb-bolt
    header_image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
    customer_industry: Travel / Insurance
    customer_employee_count: 3,700
    customer_location: The Netherlands
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: of developer teams using GitLab
        stat: 100%
      - label: projects
        stat: '322'
      - label: groups
        stat: '57'
    blurb: ANWB, the largest not-for-profit association in the Netherlands, needed a one-stop, modern toolchain to increase team autonomy and eliminate process isolation.


    introduction: |
        With GitLab, development teams at ANWB can more easily choose their own pipelines and their own processes instead of having to conform to a single way of working.


    quotes:
      - text: |
          We had developers that thought, Why would we do something else? Jenkins is fine. But I think those people need to see GitLab first and see what the difference is because GitLab is so much more than Jenkins. The power of GitLab is you can do so much more and you can make everything so much easier to manage.
        author: Michiel Crefcoeur


        author_role: Frontend Build and Release Engineer
        author_company: ANWB
      - text: |
          We are just replacing these outdated tools bit by bit, doing it the agile way. We really wanted to replace Jenkins and immediately we saw that Stash was the first one to replace because it was logical to also host your Git repositories in GitLab, and now we have started looking at other tools (f.i. XL Deploy, Nexus) that we can replace with GitLab.
        author: Ramon van de Velde
        author_role: Product Owner Beheer | P&I Online
        author_company: ANWB

    content:
      - title: Keeping the Netherlands rolling

        description: |
          What started as a small community of bike enthusiasts has grown into a full-service mobility provider over the past 135 years. The Royal Dutch Touring Club, or Algemene Nederlandse Wielrijdersbond (ANWB), was formed in 1883 when a few local cycling clubs merged. With the evolution of automobiles, the club shifted focus to roadside service. Now, in addition to roadside services, ANWB offers credit cards, car sales, bicycle maintenance, and travel services.

          Some of the club’s most popular services include route planning software for mobile devices and a connected car service that enables older cars to provide intelligence to drivers.
      - title: Replacing multiple tools and plugins with a unified toolset


        description: |
          At the beginning of 2018, the product and development teams decided to make a strategic change to improve their development process. At that time, they had multiple interconnected tooling and services. This environment was driven by developer and administrator’s knowledge and background and wasn’t easily maintained. System breakdowns required someone to manually look at what was going on. These prolonged outages prevented people from doing their jobs properly. The company operated with an attitude of  ”Okay, it’s working. Don’t touch. As long as we don’t touch it, everything will be fine.”

          The toolchain consisted of some outdated tools including Jenkins version 1 as a build server, and Stash (now known as Bitbucket Server) as a Git repository server. For artifact hosting, ANWB was using Artifactory and Nexus.


      - title: Empowering developers with a one-stop, modern toolchain


        description: |
          ANWB wanted to increase team autonomy and obliterate process isolation. Previously, their teams were using pipelines that were pieced together with various plugins. Every once in a while, a plug-in would break after a Jenkins update. Because of this, pipelines were kept as simple as possible. Now with some process shifts and using GitLab, developers run their jobs in separate pipelines and can upgrade in their own time.

          GitLab has made it easier for ANWB to manage separate teams and their individualized projects and for teams to choose their own pipelines and their own processes. Having the ability to choose their own workflows prevents teams from having to conform to a single way of working.


      - title: Looking to the future and the potential of a multi-cloud environment


        description: |
          With sights set firmly on the future, the ANWB dev team is looking to migrate from outdated backend architecture to more of a cloud-centric framework. While the path to accomplishing this hasn’t been defined yet, they know GitLab is key to this next step.

          “What we see is that GitLab really helps in seeing what’s out there because of the integrations that GitLab already provides out-of-the-box,” Crefcoeur said. “Our company, ANWB, uses both AWS and Azure but our department is not making use of those services yet. What we are doing is we’re currently experimenting with Google Cloud platform to see what GitLab has to offer on that.”

          Because of GitLab’s integrations with Knative, ANWB is exploring Google Cloud on Kubernetes.

          “We think that Kubernetes is the way to go and the integration with GitLab is really helping on that one. I think the findings are rather positive for now but we still have a long way to go because it’s pretty huge what you can do with all those new techniques,” van de Velde said.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_name: Goldman Sachs
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_name: Siemens
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_name: Fanatics
          ga_location: customers stories

